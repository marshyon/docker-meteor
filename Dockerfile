FROM ubuntu

RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup | sudo bash -
RUN apt-get update
RUN apt-get install -y git gcc make build-essential imagemagick nodejs
RUN curl https://install.meteor.com | /bin/sh
RUN npm install --silent -g meteorite
RUN mkdir meteorsrc && mkdir -p /var/www/app

ADD . ./meteorsrc
WORKDIR /meteorsrc
RUN mrt install && meteor build --directory /var/www/app

WORKDIR /var/www/app
RUN apt-get install -y python
RUN npm install fibers@1.0.1 bcrypt@0.7.7 && npm install underscore && npm install source-map-support && npm install semver

ENV PORT 3000
EXPOSE 3000
ENV ROOT_URL=http://localhost
CMD cd bundle && node ./main.js
